package fr.miage.cour.ari;

import fr.miage.cour.ari.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@SpringBootApplication
@ComponentScan({"fr.miage.cour.ari.*"})
public class ARIApplication implements ApplicationRunner {
	public static Library library;
	@Autowired
	private LibraryService libraryService;
	@Autowired
	private ReaderService readerService;

	@Autowired
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		SpringApplication.run(ARIApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) {
		Library library = new Library();
		ARIApplication.library = library;
		this.libraryService.save(library);

		Author author1 = new Author("Akira Toriyama", library);
		Author author2 = new Author("Eichiro Oda", library);
		Author author3 = new Author("Victor Hugo", library);
		Author author4 = new Author("Obata", library);
		this.libraryService.saveAllAuthors(List.of(author1, author2, author3));

		Book book1 = new Book("Dragon ball", library);
		Book book2 = new Book("Death note", library);
		Book book3 = new Comics("45", library, "One piece");
		Book book4 = new Roman("Les misérables", library);
		this.libraryService.saveAllBooks(List.of(book1, book2, book3, book4));

		this.libraryService.addBookToAuthor(author1, book1);
		this.libraryService.addBookToAuthor(author4, book2);
		this.libraryService.addBookToAuthor(author2, book3);
		this.libraryService.addBookToAuthor(author3, book4);

		Reader reader = new Reader("ADMIN", passwordEncoder().encode("ADMIN"), "admin@gmail.com", ARIApplication.library);
		reader.addAuthority("ADMIN");
		this.readerService.save(reader);
	}

}
