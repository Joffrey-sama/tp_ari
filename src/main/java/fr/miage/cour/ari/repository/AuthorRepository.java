package fr.miage.cour.ari.repository;

import fr.miage.cour.ari.model.Author;
import fr.miage.cour.ari.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    List<Author> findAllByLibrary(Library library);
}
