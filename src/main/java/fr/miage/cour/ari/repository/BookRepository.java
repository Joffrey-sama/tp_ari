package fr.miage.cour.ari.repository;

import fr.miage.cour.ari.model.Book;
import fr.miage.cour.ari.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByLibrary(Library library);
}
