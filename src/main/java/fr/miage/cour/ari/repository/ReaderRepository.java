package fr.miage.cour.ari.repository;

import fr.miage.cour.ari.model.Library;
import fr.miage.cour.ari.model.Reader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReaderRepository extends JpaRepository<Reader, Long> {
    Optional<Reader> findReaderByEmailAddress(String email);

    List<Reader> findAllByLibrary(Library library);
}
