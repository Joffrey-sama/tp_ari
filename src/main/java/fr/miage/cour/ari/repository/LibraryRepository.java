package fr.miage.cour.ari.repository;

import fr.miage.cour.ari.model.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LibraryRepository extends JpaRepository<Library, Long> {
    @Override
    Optional<Library> findById(Long id);
}
