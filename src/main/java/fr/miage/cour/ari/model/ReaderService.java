package fr.miage.cour.ari.model;

import fr.miage.cour.ari.repository.ReaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReaderService implements UserDetailsService {
    @Autowired
    private ReaderRepository readerRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Reader reader = readerRepository.findReaderByEmailAddress(s).orElseThrow(() -> new UsernameNotFoundException("Email " + s + " not found"));
        return new User(reader.getEmailAddress(), reader.getPassword(), reader.getAuthorities());
    }

    public void save(Reader reader) {
        this.readerRepository.save(reader);
    }

    // find reader by email address
    public Optional<Reader> findReaderByEmailAddress(String email) {
        return readerRepository.findReaderByEmailAddress(email);
    }

    // find all readers in library
    public List<Reader> findAllReaders(Library library) {
        return readerRepository.findAllByLibrary(library);
    }

    // find reader by id
    public Optional<Reader> findReaderById(Long id) {
        return readerRepository.findById(id);
    }

    // delete reader
    public void delete(Reader reader) {
        readerRepository.delete(reader);
    }
}
