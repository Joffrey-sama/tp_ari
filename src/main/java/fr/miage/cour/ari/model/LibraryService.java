package fr.miage.cour.ari.model;

import fr.miage.cour.ari.repository.AuthorRepository;
import fr.miage.cour.ari.repository.BookRepository;
import fr.miage.cour.ari.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LibraryService {

    @Autowired
    LibraryRepository libraryRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    AuthorRepository authorRepository;

    // find all books in library
    public List<Book> findAllBooks(Library library) {
        return bookRepository.findAllByLibrary(library);
    }

    // find book by id
    public Optional<Book> findBookById(Long id) {
        return bookRepository.findById(id);
    }

    // find all authors in library
    public Iterable<Author> findAllAuthor(Library library) {
        return authorRepository.findAllByLibrary(library);
    }

    // find author by id
    public Optional<Author> findAuthorById(Long id) {
        return authorRepository.findById(id);
    }

    // find library by id
    public Optional<Library> findLibraryById(Long id) {
        return libraryRepository.findById(id);
    }

    public void delete(Author author) {
        this.authorRepository.delete(author);
    }

    public void delete(Book book) {
        this.bookRepository.delete(book);
    }

    public void save(Author author) {
        this.authorRepository.save(author);
    }

    public void save(Book book) {
        this.bookRepository.save(book);
    }

    public void save(Library library) {
        this.libraryRepository.save(library);
    }

    public void saveAllAuthors(List<Author> authors) {
        this.authorRepository.saveAll(authors);
    }

    public void saveAllBooks(List<Book> books) {
        this.bookRepository.saveAll(books);
    }

    // add a book to author's list of books
    public void addBookToAuthor(Author author, Book book) {
        author.addBook(book);
        save(author);
    }

    // remove a book from author's list of books
    public void removeBookFromAuthor(Author author, Book book) {
        author.removeBook(book);
        save(author);
    }
}