package fr.miage.cour.ari.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Comics")
public class Comics extends Book {
    String series;

    public Comics(String title, Library library, String series) {
        super(title, library);
        this.series = series;
    }

    public Comics() {

    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
