package fr.miage.cour.ari.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Roman")
public class Roman extends Book {

    public Roman(String title, Library library) {
        super(title, library);
    }

    public Roman() {

    }
}
