package fr.miage.cour.ari.controller;

import fr.miage.cour.ari.ARIApplication;
import fr.miage.cour.ari.model.Reader;
import fr.miage.cour.ari.model.ReaderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ReaderController {

    private final ReaderService readerService;

    ReaderController(ReaderService readerService) {
        this.readerService = readerService;
    }

    // redirect to home view
    @GetMapping("/")
    public ModelAndView home(ModelAndView model) {
        model.setViewName("home");
        return model;
    }

    // redirect to signup view
    @GetMapping("/signup")
    public ModelAndView signUp(ModelAndView model) {
        model.setViewName("signup");
        return model;
    }

    // signup: create reader and redirect to reader home
    @PostMapping("/signup")
    public ModelAndView signup(@ModelAttribute("reader") Reader reader, BindingResult result, ModelAndView model, HttpSession session, HttpServletRequest request) throws ServletException {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (result.hasErrors()) {
            model.setViewName("home");
            return model;
        }
        String password = reader.getPassword();
        reader.setPassword(encoder.encode(reader.getPassword()));
        reader.setLibrary(ARIApplication.library);
        readerService.save(reader);
        session.setAttribute("user", reader);
        model.setViewName("readerHome");
        request.login(reader.getEmailAddress(), password);
        return model;
    }

    // redirect to user home (admin or reader)
    @GetMapping("/home")
    public ModelAndView userHome(ModelAndView model, Authentication authentication, HttpSession session) {
        String email = authentication.getName();
        Reader reader = readerService.findReaderByEmailAddress(email).orElseThrow(() -> new UsernameNotFoundException("Email " + email + " not found"));

        if (session.getAttribute("user") == null) session.setAttribute("user", reader);
        if (reader.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) model.setViewName("adminHome");
        else model.setViewName("readerHome");
        return model;
    }

    // redirect to account: not implemented
    @GetMapping("/account")
    public ResponseEntity<String> userAccount() {
        return new ResponseEntity<>("Not implemented", HttpStatus.NOT_IMPLEMENTED);
    }

    // redirect to readers admin view
    @GetMapping("/admin/readers")
    public ModelAndView manageReaders(ModelAndView model) {
        model.setViewName("manageReaders");
        model.addObject("readers", this.readerService.findAllReaders(ARIApplication.library));
        return model;
    }

    // create reader and redirect to reader admin view
    @PostMapping("/admin/newReader")
    public String newReader(@ModelAttribute("reader") Reader reader, BindingResult result) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (result.hasErrors()) {
            return "redirect:/admin/readers";
        }
        reader.setPassword(encoder.encode(reader.getPassword()));
        reader.setLibrary(ARIApplication.library);
        readerService.save(reader);
        return "redirect:/admin/readers";
    }

    // delete reader and redirect to reader admin view
    @PostMapping("/admin/reader/delete/{id}")
    public String deleteBook(@PathVariable("id") Long id) throws Exception {
        Reader reader = readerService.findReaderById(id).orElseThrow(() -> new Exception("no book for id: " + id));
        readerService.delete(reader);
        return "redirect:/admin/readers";
    }

    // redirect to modify reader admin view
    @GetMapping("/admin/reader/modify/{id}")
    public ModelAndView modifyReader(ModelAndView model, @PathVariable Long id) throws Exception {
        Reader reader = readerService.findReaderById(id).orElseThrow(() -> new Exception("Reader not found"));
        model.setViewName("modifyReader");
        model.addObject("reader", reader);
        return model;
    }

    // modify reader and redirect to reader admin view
    @PostMapping("/admin/reader/modify/{id}")
    public String modifyReader(@PathVariable("id") Long id, @ModelAttribute("reader") Reader newReader, BindingResult result) throws Exception {
        Reader reader = readerService.findReaderById(id).orElseThrow(() -> new Exception("Reader not found"));
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (result.hasErrors()) {
            return "redirect:/admin/readers";
        }
        reader.setName(newReader.getName());
        reader.setEmailAddress(newReader.getEmailAddress());
        reader.setPassword(encoder.encode(newReader.getPassword()));
        reader.setLibrary(ARIApplication.library);
        readerService.save(reader);
        return "redirect:/admin/readers";
    }
}
