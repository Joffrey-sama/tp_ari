package fr.miage.cour.ari.controller;

import fr.miage.cour.ari.ARIApplication;
import fr.miage.cour.ari.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LibraryController {

    @Autowired
    LibraryService libraryService;

    // redirect to list of books view
    @GetMapping("/books")
    public ModelAndView books(ModelAndView model) {
        Map<String, Object> allBooks = new HashMap<>();
        allBooks.put("allBooks", libraryService.findAllBooks(ARIApplication.library));
        model.addAllObjects(allBooks);
        model.setViewName("books");
        return model;
    }

    // redirect to book details view: not implemented
    @GetMapping("/book/{id}")
    public ResponseEntity<String> bookDetails(@PathVariable String id) {
        return new ResponseEntity<>("Not implemented", HttpStatus.NOT_IMPLEMENTED);
    }

    // redirect to list of authors view
    @GetMapping("/authors")
    public ModelAndView authors(ModelAndView model) {
        Map<String, Object> allAuthors = new HashMap<>();
        allAuthors.put("allAuthors", libraryService.findAllAuthor(ARIApplication.library));
        model.addAllObjects(allAuthors);
        model.setViewName("authors");
        return model;
    }

    // redirect to book details view: not implemented
    @GetMapping("/author/{id}")
    public ResponseEntity<String> authorDetails(@PathVariable String id) {
        return new ResponseEntity<>("Not implemented", HttpStatus.NOT_IMPLEMENTED);
    }

    // redirect to author admin view
    @GetMapping("/admin/authors")
    public ModelAndView manageAuthors(ModelAndView model) {
        model.setViewName("manageAuthors");
        return model;
    }

    // redirect to book admin view
    @GetMapping("/admin/books")
    public ModelAndView manageBooks(ModelAndView model) {
        model.setViewName("manageBooks");
        model.addObject("authors", this.libraryService.findAllAuthor(ARIApplication.library));
        return model;
    }

    // create new author and redirect to author admin view
    @PostMapping("/admin/newAuthor")
    public String newAuthor(Author author) {
        author.setLibrary(ARIApplication.library);
        libraryService.save(author);
        return "redirect:/authors";
    }

    // create new book and redirect to book admin view
    @PostMapping("/admin/newBook")
    public String newBook(@RequestParam("title") String title, @RequestParam("authors") List<Author> authors, @RequestParam("type") String type) {
        Book book;
        switch (type) {
            case "Comics":
                book = new Comics();
                break;
            case "Roman":
                book = new Roman();
                break;
            default:
                book = new Book();
                break;
        }
        book.setTitle(title);
        book.setAuthors(authors);
        book.setLibrary(ARIApplication.library);
        libraryService.save(book);
        for (Author author : authors) {
            libraryService.addBookToAuthor(author, book);
        }
        return "redirect:/books";
    }

    // borrow book and redirect to book admin view
    @PostMapping("/book/borrow/{id}")
    public ResponseEntity<String> borrowBook(@PathVariable("id") Long id) {
        return new ResponseEntity<>("Not implemented", HttpStatus.NOT_IMPLEMENTED);
    }

    // delete book and redirect to book admin view
    @PostMapping("/admin/book/delete/{id}")
    public String deleteBook(@PathVariable("id") Long id) throws Exception {
        Book book = libraryService.findBookById(id).orElseThrow(() -> new Exception("no book for id: " + id));
        List<Author> authors = book.getAuthors();
        for (Author author : authors) {
            libraryService.removeBookFromAuthor(author, book);
        }
        libraryService.delete(book);
        return "redirect:/books";
    }

    // delete author and redirect to author admin view
    @PostMapping("/admin/author/delete/{id}")
    public String deleteAuthor(@PathVariable("id") Long id) throws Exception {
        Author author = libraryService.findAuthorById(id).orElseThrow(() -> new Exception("no book for id: " + id));
        libraryService.delete(author);
        return "redirect:/books";
    }

    // delete book and redirect to book admin view
    @PostMapping("/admin/book/modify/{id}")
    public ResponseEntity<String> modifyBook(@PathVariable("id") Long id) {
        return new ResponseEntity<>("Not implemented", HttpStatus.NOT_IMPLEMENTED);
    }

    // delete book and redirect to book admin view
    @PostMapping("/admin/author/modify/{id}")
    public ResponseEntity<String> modifyAuthor(@PathVariable("id") Long id) {
        return new ResponseEntity<>("Not implemented", HttpStatus.NOT_IMPLEMENTED);
    }

}
